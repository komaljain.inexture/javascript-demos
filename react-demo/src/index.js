import React from "react";
import ReactDOM from 'react-dom';
import './index.css';
import Card from './components/Cards.js';
import Sdata from './components/Sdata.js';



ReactDOM.render(
    <>
    <h1 className="heading_style">Let's Start With React</h1>
    {Sdata.map ((val) =>
    {
      return(
        <Card 
          imgsrc={val.imgsrc}
          sname={val.sname}
          title ={val.title}
          link={val.link}
        />
       
      );
    })}
    </>,
    document.getElementById('root')
);


