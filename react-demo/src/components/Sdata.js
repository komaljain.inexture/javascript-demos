const Sdata = [
    {
        sname: "Components",
        imgsrc: "https://www.dmcinfo.com/Portals/0/Blog%20Pictures/React%20Components.png",
        title : "React Components",
        link: "https://www.youtube.com/watch?v=YcXVT2udeu8"
    },
    {
        sname: "Props",
        imgsrc: "https://miro.medium.com/max/1138/1*27LtOtFyJe7MguQkNcZQjQ.png",
        title : "React Props",
        link: "https://www.youtube.com/watch?v=HRhJVGjIraE&t=334s&ab_channel=ThapaTechnicalThapaTechnicalVerified"
    },
    {
        sname: "State",
        imgsrc: "https://miro.medium.com/max/1200/1*hYSKyofnqThnPIsYRfnUUQ.png",
        title: "React State",
        link :"https://www.youtube.com/watch?v=4ORZ1GmjaMc"
    },
    {
        sname: "Handling Events",
        imgsrc: "https://i.morioh.com/201022/4518834f.webp",
        title: "React Handling Events",
        link: "https://www.youtube.com/watch?v=Znqv84xi8Vs&ab_channel=CodevolutionCodevolution"
    },
    {
        sname: "Forms",
        imgsrc: "https://i.ytimg.com/vi/t3r9xW-sxqs/maxresdefault.jpg",
        title: "React Forms",
        link: "https://www.youtube.com/watch?v=7Vo_VCcWupQ"
    },
    {
        sname: "Array Map",
        imgsrc: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZiP-tCQxTVRkWKOcuZ2GYWPgPaSiqdN8Rfg&usqp=CAU",
        title : "React Array Map",
        link: "https://www.youtube.com/watch?v=TJ66JKNdBtE&ab_channel=CodeStepByStep"
    },
];

export default Sdata;